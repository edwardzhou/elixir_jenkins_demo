defmodule ElixirJenkinsDemo.Repo do
  use Ecto.Repo,
    otp_app: :elixir_jenkins_demo,
    adapter: Ecto.Adapters.Postgres
end
