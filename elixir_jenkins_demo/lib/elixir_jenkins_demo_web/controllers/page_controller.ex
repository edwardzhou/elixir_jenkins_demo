defmodule ElixirJenkinsDemoWeb.PageController do
  use ElixirJenkinsDemoWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
