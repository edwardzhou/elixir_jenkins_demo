# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :elixir_jenkins_demo,
  ecto_repos: [ElixirJenkinsDemo.Repo]

# Configures the endpoint
config :elixir_jenkins_demo, ElixirJenkinsDemoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "SJiI5nbWvpiJ16eGQy2hrRTsGiIqW6wmOoAuixDfEZyL7ZXBZu27xKvFzizL0bji",
  render_errors: [view: ElixirJenkinsDemoWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: ElixirJenkinsDemo.PubSub,
  live_view: [signing_salt: "g5qnTXA5"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
